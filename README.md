# 目录
### 有好书给我留言，我会Push的！！！

- [Java](Java)
    - `Java语言`
        - [EffectiveJava中文版第一版](Java/Java语言/EffectiveJava中文版第一版.pdf)
        - [EffectiveJava中文版第二版](Java/Java语言/EffectiveJava中文版第二版.pdf)
        - [EffectiveJava英文版第二版](Java/Java语言/EffectiveJava英文版第二版.pdf)
        - [Java技术手册(第5版)](Java/Java语言/JAVA技术手册(第5版).pdf)
        - [Java核心技术 卷I 基础知识 中文版第9版](Java/Java语言/Java核心技术%20卷I%20基础知识%20中文版第9版.pdf)
        - [Java核心技术 卷Ⅱ 高级特征 中文版第9版](Java/Java语言/Java核心技术%20卷Ⅱ%20高级特征%20中文版第9版.pdf)
        - [Java编程思想(第3版)](Java/Java语言/Java编程思想(第3版).pdf)
        - [Java编程思想(第4版)](Java/Java语言/Java编程思想(第4版).pdf)
        - [Java编程思想习题答案-书签版本](Java/Java语言/Java编程思想习题答案-书签版本.pdf)
        - [Java解惑（中文）](Java/Java语言/Java解惑（中文）.pdf)
        - [jls8(Java语言规范)](Java/Java语言/jls8(Java语言规范).pdf)
        - [Thinking_in_Java_3th_edition](Java/Java语言/Thinking_in_Java_3th_edition.pdf)
        - [Thinking_in_Java_4th_edition](Java/Java语言/Thinking_in_Java_4th_edition.pdf)                
        
    - `JVM`
        - [HotSpot实战](Java/JVM/HotSpot实战.pdf)
        - [Java虚拟机规范（Java SE 7）](Java/JVM/Java虚拟机规范（Java%20SE%207）.pdf)
        - [《自己动手写Java虚拟机》-张秀宏](Java/JVM/《自己动手写Java虚拟机》-张秀宏.pdf)
        - [垃圾回收算法手册 自动内存管理的艺术（英）理查德·琼斯](Java/JVM/垃圾回收算法手册%20自动内存管理的艺术%20,（英）理查德·琼斯%20sample.pdf)
        - [垃圾收集-Garbage.Collection(美)Richard.Jones&Rafael.Lins](Java/JVM/垃圾收集-Garbage.Collection(美)Richard.Jones&Rafael.Lins.pdf)
        - [实战Java虚拟机  JVM故障诊断与性能优化](Java/JVM/实战Java虚拟机%20%20JVM故障诊断与性能优化.pdf)
        - [深入Java虚拟机-第二版-Bill Venners](Java/JVM/深入Java虚拟机-第二版-Bill%20Venners.pdf)
        - [深入理解JAVA内存模型](Java/JVM/深入理解JAVA内存模型.pdf)
        - [深入理解Java虚拟机 JVM高级特性与最佳实践-周志明第一版](Java/JVM/深入理解Java虚拟机%20JVM高级特性与最佳实践-周志明第一版.pdf)
   
    - `JavaIO`
        - [Java NIO-Ron Hitchens-中文](Java/JavaIO/Java%20NIO-Ron%20Hitchens-中文%20.pdf)
        - [JavaIO-英文](Java/JavaIO/Java%20NIO-Ron%20Hitchens-中文%20.pdf)
        
    - `Java多线程`
        - [Java 并发编程实战](Java/Java并发/Java%20并发编程实战.pdf)
        - [Java 7并发编程实战手册](Java/Java并发/Java%207并发编程实战手册.pdf)
        - [Java多线程编程实战指南 设计模式篇](Java/Java并发/Java多线程编程实战指南%20设计模式篇.pdf)
        - [Java多线程编程核心技术](Java/Java并发/Java多线程编程核心技术.pdf)
        - [Java并发编程-核心方法与框架-高洪岩著](Java/Java并发/JAVA并发编程-核心方法与框架-高洪岩著.pdf)
        - [Java并发编程的艺术](Java/Java并发/Java并发编程的艺术.pdf)
        - [Java并发编程设计原则与模式-DougLea](Java/Java并发/Java并发编程设计原则与模式-DougLea.pdf)
        - [Java线程 第三版 中文版 oreilly](Java/Java并发/JAVA线程%20第三版%20中文版%20oreilly.pdf)
        - [实战Java高并发程序设计-葛一鸣 ](Java/Java并发/实战Java高并发程序设计-葛一鸣%20.pdf)
        - [Java虚拟机并发编程](Java/Java并发/《Java虚拟机并发编程》.pdf)
        - [Programming Concurrency on the JVM](Java/Java并发/Programming%20Concurrency%20on%20the%20JVM.pdf)
        
- [JavaScript](JavaScript)



- [J2EE](J2EE)
        
        
- [C/C++](C&C++)


- [模式](Patterns)


- [操作系统](OS)

    - `linux`
        - [flex与bison](OS/linux/flex与bison.pdf)
        - [lex与yacc](OS/linux/lex与yacc.pdf)

- [人文](Cultural)

    - `人物传记`
        - [Linus自传-乐者为王](Cultural/Just_For_Fun-Linus_Torvalds自传_乐者为王—中文版.pdf)
    
    - `历史`
    
    - `管理`
        - [人件集 人性化的软件开发](Cultural/管理/人件集%20人性化的软件开发.pdf)
        - [人月神话](Cultural/管理/人月神话.pdf)
        - [你的灯亮着吗](Cultural/管理/你的灯亮着吗.pdf)
        - [技术领导之路_全面解决问题的途径](Cultural/管理/技术领导之路_全面解决问题的途径.pdf)
        - [程序开发心理学](Cultural/管理/程序开发心理学.pdf)
  
    - `互联网与产业`
        - [沸腾十五年：中国互联网1995-2009 林军着](Cultural/互联网与产业/1%20沸腾十五年：中国互联网1995-2009%20林军着.pdf)
        - [信息规则：网络经济的策略指导](Cultural/互联网与产业/2%20信息规则：网络经济的策略指导.pdf)
        - [失控：全人类的最终命运和结局](Cultural/互联网与产业/3%20失控：全人类的最终命运和结局.pdf)
        - [技术元素 凯文·凯利](Cultural/互联网与产业/4%20技术元素%20凯文·凯利.pdf)
        - [浪潮之巅](Cultural/互联网与产业/5%20浪潮之巅.pdf)
        - [黑客与画家](Cultural/互联网与产业/6%20黑客与画家.pdf)
        - [链接：网络新科学 巴拉巴西](Cultural/互联网与产业/7%20链接：网络新科学%20巴拉巴西.pdf)
        - [正在爆发的互联网革命](Cultural/互联网与产业/8%20正在爆发的互联网革命.pdf)
        - [免费：商业的未来](Cultural/互联网与产业/9%20免费：商业的未来.pdf)
        - [FACEBOOK效应](Cultural/互联网与产业/10%20FACEBOOK效应.pdf)
        - [O2O移动互联网时代的商业革命](Cultural/互联网与产业/11%20O2O移动互联网时代的商业革命.pdf)
        - [轻公司](Cultural/互联网与产业/12%20轻公司.pdf)
        - [数据挖掘技术](Cultural/互联网与产业/13%20数据挖掘技术.pdf)
        - [移动浪潮](Cultural/互联网与产业/14%20移动浪潮.pdf)
        - [世界是平的](Cultural/互联网与产业/15%20世界是平的.pdf)
        - [大数据时代](Cultural/互联网与产业/16%20大数据时代.pdf)
        - [大数据时代的历史机遇](Cultural/互联网与产业/17%20大数据时代的历史机遇%20（不全）.pdf)
        - [撬动地球的Google](Cultural/互联网与产业/18%20撬动地球的Google.pdf)
        - [谷歌将带来什么](Cultural/互联网与产业/19%20谷歌将带来什么.pdf)
        - [长尾理论](Cultural/互联网与产业/20%20长尾理论.pdf)